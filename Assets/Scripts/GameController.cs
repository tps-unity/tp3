using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using TMPro;

public class GameController : MonoBehaviour
{
    public float SpawnEnnemyCooldown = 0.5f;
    public GameObject[] EnnemyPrefabs;
    [SaveField] public float CurrentStage = 1f;
    [SaveField] public int EnnemiesKilledInCurrentStage = 0;
    [SaveField] public int TotalEnnemiesKilled = 0;
    [SaveField] public float HealthModifier = 1f;
    public float HealthModifierMultiplier = 1.1f;
    [SaveField] public float NumEnnemiesPerStage = 5f;
    public float NumEnnemiesPerStageIncrement = 1f;
    public float RandomEnnemySpawnRate = 1f;
    public float RandomEnnemySpawnChance = 0.01f;
    public float RandomEnnemySpawnLeftX = -8.2f;
    public GameObject RandomEnnemyPrefab;
    public int RerollCost = 5;

    public UIController ui;
    BackgroundController backgroundController;
    Player player;
    Ennemy currentEnnemy = null;
    Ennemy.EType nextEnnemyType;
    float spawnEnnemyTime = 0f;
    bool spawningEnnemy = false;
    bool changingStage = false;
    float randomEnnemySpawnTime = 0f;
    GameObject spawnZone;

    TMP_Text CurrentStageText;
    TMP_Text EnnemiesKilledInCurrentStageText;
    TMP_Text TotalEnnemiesKilledText;
     Canvas canvas;

    void Start()
    {
        ui = GameObject.Find("UIController").GetComponent<UIController>();
        player = GameObject.Find("Player").GetComponent<Player>();
        backgroundController = GameObject.Find("BackgroundController").GetComponent<BackgroundController>();
        spawnZone = GameObject.Find("SpawnZone");

        LoadGame();

        nextEnnemyType = EnnemiesKilledInCurrentStage >= NumEnnemiesPerStage ? Ennemy.EType.Boss : Ennemy.EType.Default;
        TrySpawnEnnemy(true);

        TotalEnnemiesKilledText = GameObject.Find("TotalEnnemiesKilled").GetComponent<TMP_Text>();
        CurrentStageText = GameObject.Find("CurrentStage").GetComponent<TMP_Text>();
        EnnemiesKilledInCurrentStageText = GameObject.Find("EnnemiesKilledInCurrentStage").GetComponent<TMP_Text>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    }

    void Update()
    {
        TrySpawnEnnemy();
        TrySpawnRandomEnnemy();

        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveGame();
        }

        TotalEnnemiesKilledText.text = TotalEnnemiesKilled.ToString();
        CurrentStageText.text = CurrentStage.ToString();
        EnnemiesKilledInCurrentStageText.text = EnnemiesKilledInCurrentStage.ToString();
    }

    void SaveGame()
    {
        SaveSystem<GameController>.Save(this);
        SaveSystem<Player>.Save(player);
    }

    void LoadGame()
    {
        SaveSystem<GameController>.Load(this);
        SaveSystem<Player>.Load(player);
    }

    /* Game Flow */

    public void NextStage()
    {
        CurrentStage++;
        HealthModifier *= HealthModifierMultiplier;
        NumEnnemiesPerStage += NumEnnemiesPerStageIncrement;
        EnnemiesKilledInCurrentStage = 0;

        changingStage = true;
        SpawnEnnemyLater();
    }

    /* Ennemy */

    void OnEnnemyDeath(Ennemy ennemy)
    {
        int goldBonus = Random.Range(0f, 1f) <= player.GoldChance ? 1 : 0;

        player.Gold += ennemy.GoldReward + goldBonus;
        EnnemiesKilledInCurrentStage++;
        TotalEnnemiesKilled++;

        currentEnnemy = null;

        if (ennemy.Type == Ennemy.EType.Boss)
        {
            ui.ShowBuyPanel();
            return;
        }

        SpawnEnnemyLater(EnnemiesKilledInCurrentStage >= NumEnnemiesPerStage ? Ennemy.EType.Boss : Ennemy.EType.Default);
    }

    public void SpawnEnnemyLater(Ennemy.EType type = Ennemy.EType.Default)
    {
        int nextBg = backgroundController.CurrentBackground;

        if (changingStage)
        {
            if (++nextBg >= backgroundController.backgrounds.Length)
            {
                nextBg = 0;
            }

            changingStage = false;
        }

        backgroundController.SetBackground(nextBg, false);

        nextEnnemyType = type;
        spawnEnnemyTime = 0f;
        spawningEnnemy = true;
    }

    void TrySpawnEnnemy(bool now = false)
    {
        if (!now)
        {
            if (!spawningEnnemy)
            {
                return;
            }

            if (spawnEnnemyTime < SpawnEnnemyCooldown)
            {
                spawnEnnemyTime += Time.deltaTime;
                return;
            }
        }

        currentEnnemy = Instantiate(EnnemyPrefabs[Random.Range(0, EnnemyPrefabs.Length - 1)]).GetComponent<Ennemy>();
        currentEnnemy.Type = nextEnnemyType;
        currentEnnemy.OnDeath.AddListener(OnEnnemyDeath);
        currentEnnemy.transform.position = new Vector2(0f, -2f);

        if (currentEnnemy.Type == Ennemy.EType.Boss)
        {
            currentEnnemy.transform.localScale = new Vector2(1.5f, 1.5f);
            currentEnnemy.transform.position = new Vector2(0f, -3f);
        }

        spawningEnnemy = false;
    }

    void TrySpawnRandomEnnemy()
    {
        if (currentEnnemy == null)
        {
            return;
        }

        randomEnnemySpawnTime += Time.deltaTime;

        if (randomEnnemySpawnTime >= RandomEnnemySpawnRate)
        {
            randomEnnemySpawnTime = 0f;

            if (Random.Range(0f, 1f) <= RandomEnnemySpawnChance)
            {
                // Spawn random ennemy

                Transform zone = spawnZone.transform;

                float offset = (zone.localScale.y / 2f) - 0.5f;
                float y = Random.Range(zone.position.y - offset, zone.position.y + offset);

                RandomEnnemy randomEnnemy = Instantiate(RandomEnnemyPrefab).GetComponent<RandomEnnemy>();
                randomEnnemy.transform.position = new Vector2(RandomEnnemySpawnLeftX, y);
                randomEnnemy.OnClick.AddListener(RandomEnnemyClicked);
            }
        }
    }

    void RandomEnnemyClicked()
    {
        player.Gold += 1;
    }
}
