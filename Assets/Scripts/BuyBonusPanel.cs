using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BuyBonusPanel : MonoBehaviour
{
    public Bonus bonus = null;

    TMP_Text goldCost;
    TMP_Text description;
    Button buyButton;
    Player player;
    Image background;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();

        goldCost = transform.Find("GoldCostImage/GoldCost").GetComponent<TMP_Text>();
        description = transform.Find("Description").GetComponent<TMP_Text>();
        buyButton = transform.Find("BuyButton").GetComponent<Button>();
        background = GetComponent<Image>();

        buyButton.onClick.AddListener(Buy);
    }

    void Update()
    {
        if (bonus != null)
        {
            background.color = Bonus.RARITY_COLORS[bonus.Rarity].WithAlpha(100f / 255f);
            goldCost.text = bonus.Cost.ToString();
            description.text = bonus.Description;
        }
        
        buyButton.enabled = bonus != null && player.Gold >= bonus.Cost;
    }

    void Buy()
    {
        player.BuyBonus(bonus);
        gameObject.SetActive(false);
    }
}
