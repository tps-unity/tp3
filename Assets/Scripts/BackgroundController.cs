using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundController : MonoBehaviour
{
    public int CurrentBackground;
    public float TransitionSpeed = 10f;
    public Sprite[] backgrounds; 

    SpriteRenderer currentSpriteRenderer, nextSpriteRenderer;
    int nextBackground = -1;
    float baseX, rightX;

    void Start()
    {
        currentSpriteRenderer = GetComponent<SpriteRenderer>();
        nextSpriteRenderer = GameObject.Find("NextBackground").GetComponent<SpriteRenderer>();

        baseX = currentSpriteRenderer.transform.position.x;
        rightX = nextSpriteRenderer.transform.position.x;

        SetBackground(0, true);
    }

    void Update()
    {
        if (nextBackground != -1)
        {
            float translateX = Time.deltaTime * TransitionSpeed;

            if (nextSpriteRenderer.transform.position.x > baseX)
            {
                currentSpriteRenderer.transform.Translate(Vector2.left * translateX);
                nextSpriteRenderer.transform.Translate(Vector2.left * translateX);
            }
            else
            {
                SetBackground(nextBackground, true);
                currentSpriteRenderer.transform.position = new Vector2(baseX, currentSpriteRenderer.transform.position.y);
                nextBackground = -1;
            }
        }
    }

    public void SetBackground(int index, bool now)
    {
        if (now)
        {
            CurrentBackground = index;
            currentSpriteRenderer.sprite = backgrounds[index];

            return;
        }

        nextSpriteRenderer.transform.position = new Vector2(rightX, currentSpriteRenderer.transform.position.y);
        nextSpriteRenderer.sprite = backgrounds[index];
        nextBackground = index;
    }
}
