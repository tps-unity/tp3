using System;
using UnityEngine;

public class SaveField : Attribute {}

public class SaveSystem<T> where T : new()
{
    public static void Save(T obj)
    {
        Type type = typeof(T);
        
        Debug.Log($"Saving {type.Name}...");

        foreach (var field in type.GetFields())
        {
            string name = $"{type.Name}.{field.Name}";

            if (field.IsDefined(typeof(SaveField), false))
            {
                if (field.FieldType == typeof(float))
                {
                    PlayerPrefs.SetFloat(name, (float)field.GetValue(obj));
                }
                else if (field.FieldType == typeof(string))
                {
                    PlayerPrefs.SetString(name, (string)field.GetValue(obj));
                }
                else if (field.FieldType == typeof(int))
                {
                    PlayerPrefs.SetInt(name, (int)field.GetValue(obj));
                }
            }
        }

        PlayerPrefs.Save();

        Debug.Log($"{type.Name} saved");
    }

    public static void Load(T obj)
    {
        Type type = typeof(T);

        Debug.Log($"Loading {type.Name}...");

        foreach (var field in type.GetFields())
        {
            if (field.IsDefined(typeof(SaveField), false))
            {
                T defaultObject = new();
                string name = $"{type.Name}.{field.Name}";

                if (field.FieldType == typeof(float))
                {
                    field.SetValue(obj, PlayerPrefs.GetFloat(name , (float)field.GetValue(defaultObject)));
                }
                else if (field.FieldType == typeof(string))
                {
                    field.SetValue(obj, PlayerPrefs.GetString(name, (string)field.GetValue(defaultObject)));
                }
                else if (field.FieldType == typeof(int))
                {
                    field.SetValue(obj, PlayerPrefs.GetInt(name, (int)field.GetValue(defaultObject)));
                }
            }
        }

        Debug.Log($"{type.Name} loaded");
    }
}
