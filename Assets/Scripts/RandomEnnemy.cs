using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class RandomEnnemy : MonoBehaviour, IPointerClickHandler
{
    public float Speed = 10f;
    public UnityEvent OnClick = new();

    Animator animator;
    AnimationController animationController;
    bool dead = false;

    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("Running", true);

        animationController = animator.GetBehaviour<AnimationController>();
        animationController.OnAnimationEnd.AddListener(OnAnimationEnd);
    }

    void Update()
    {
        if (!dead)
        {
            transform.Translate(Speed * Time.deltaTime * Vector2.right);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "SpawnZone")
        {
            Destroy(gameObject);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        dead = true;
        
        animator.SetBool("Running", false);
        animator.SetTrigger("DieTrigger");
    }

    void OnAnimationEnd(AnimatorStateInfo state)
    {
        if (state.IsName("Dying"))
        {
            Destroy(gameObject);
            OnClick.Invoke();
        }
    }
}
