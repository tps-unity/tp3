

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bonus
{
    public enum EType
    {
        Damage,
        PassiveDamage,
        BonusGoldChance,
        CritChance,
        CritDamage,
        HitCooldownReduction
    }

    public enum ERarity
    {
        Common,
        Rare,
        Epic,
        Legendary
    }

    public EType Type;
    public ERarity Rarity;
    public int Cost = 0;
    public string Description = "";
    public float Value = 0f;

    public Bonus(EType type, ERarity rarity, string description, int cost, float value)
    {
        Type = type;
        Rarity = rarity;
        Description = description;
        Cost = cost;
        Value = value;
    }

    public Bonus(Bonus other)
    {
        Type = other.Type;
        Rarity = other.Rarity;
        Description = other.Description;
        Cost = other.Cost;
        Value = other.Value;
    }

    // override object.Equals
    public override bool Equals(object obj)
    {
        //
        // See the full list of guidelines at
        //   http://go.microsoft.com/fwlink/?LinkID=85237
        // and also the guidance for operator== at
        //   http://go.microsoft.com/fwlink/?LinkId=85238
        //
        
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Bonus other = (Bonus)obj;
        
        return Type == other.Type
            && Value == other.Value
            && Rarity == other.Rarity
            && Cost == other.Cost;
    }
    
    public override int GetHashCode()
    {
        return Type.GetHashCode()
            ^ Value.GetHashCode()
            ^ Rarity.GetHashCode()
            ^ Cost.GetHashCode();
    }

    static public bool operator==(Bonus first, Bonus second)
    {
        return first.Equals(second);
    }

    static public bool operator!=(Bonus first, Bonus second)
    {
        return !(first == second);
    }

    public static readonly Bonus[] BONUSES = {
        new(EType.Damage, ERarity.Common, "+2 de dégats des clics", 10, 2f),
        new(EType.PassiveDamage, ERarity.Common, "+1 de dégats passif", 10, 1f),
        new(EType.BonusGoldChance, ERarity.Common, "+5% de chance de gold supplémentaire", 10, 0.05f),
        new(EType.CritChance, ERarity.Common, "+1% de chances de coup critique", 10, 0.01f),
        new(EType.CritDamage, ERarity.Common, "+10% de dégats des coups critiques", 10, 0.1f),
        new(EType.HitCooldownReduction, ERarity.Rare, "+5% vitesse de clic", 25, 0.05f)
    };

    public static readonly Dictionary<ERarity, float> BONUSES_DROP_RATES = new() {
        { ERarity.Common, 0.65f },
        { ERarity.Rare, 0.2f },
        { ERarity.Epic, 0.1f },
        { ERarity.Legendary, 0.05f }
    };

    public static readonly Dictionary<ERarity, Color> RARITY_COLORS = new() {
        { ERarity.Common, new Color(1f, 1f, 1f) },
        { ERarity.Rare, new Color(100f / 255f, 214f / 255f, 1f) },
        { ERarity.Epic, new Color(1f, 119f / 255f, 1f) },
        { ERarity.Legendary, new Color(1f, 139f / 255f, 67f / 255f) }
    };

    public static Bonus[] GetRandomBonuses(int count)
    {
        var currentBonuses = BONUSES.ToList();

        List<Bonus> bonuses = new();

        for (int i = 0; i < count; ++i)
        {
            bonuses.Add(GetRandomBonus(currentBonuses));
        }

        return bonuses.ToArray();
    }

    static Bonus GetRandomBonus(List<Bonus> currentBonuses)
    {
        float rand = UnityEngine.Random.Range(0f, 1f);

        if (rand <= BONUSES_DROP_RATES[ERarity.Legendary])
        {
            return GetRandomBonus(ERarity.Legendary, currentBonuses);
        }
        else if (rand <= BONUSES_DROP_RATES[ERarity.Epic])
        {
            return GetRandomBonus(ERarity.Epic, currentBonuses);
        }
        else if (rand <= BONUSES_DROP_RATES[ERarity.Rare])
        {
            return GetRandomBonus(ERarity.Rare, currentBonuses);
        }
        else // Common
        {
            return GetRandomBonus(ERarity.Common, currentBonuses);
        }
    }

    static Bonus GetRandomBonus(ERarity rarity, List<Bonus> currentBonuses)
    {
        var bonuses = currentBonuses.Where(bonus => bonus.Rarity == rarity).ToList();

        if (bonuses.Count == 0)
        {
            return GetRandomBonus(rarity - 1, currentBonuses); // Fallback to lower rarity
        }

        var bonus = bonuses[UnityEngine.Random.Range(0, bonuses.Count)];
        currentBonuses.RemoveAll(b => b == bonus);

        return bonus;
    }
}