using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [HideInInspector] public float Health = 0f;
    [HideInInspector] public float MaxHealth { get => maxHealth; set { maxHealth = value; displayHealth = value; } }
    public float HealthBarSpeed = 10f;
    public GameObject buyPanel;
    public BuyBonusPanel[] buyBonusPanels;
    public float DamageIndicatorDuration;

    Image healthBar;
    TMP_Text healthText;
    float displayHealth = 0f;
    float maxHealth = 0f;
    
    void Start()
    {
        healthText = GameObject.Find("HealthText").GetComponent<TMP_Text>();

        healthBar = GameObject.Find("HealthBar").GetComponent<Image>();
    }

    void Update()
    {
        if (displayHealth > Health)
        {
            float newDisplayHealth = Mathf.Lerp(displayHealth, Health, Time.deltaTime * HealthBarSpeed);

            displayHealth = Math.Max(newDisplayHealth, 0f);
        }

        healthBar.fillAmount = displayHealth / maxHealth;

        healthText.text = $"{Math.Ceiling(Health)} / {Math.Ceiling(maxHealth)}";
    }

    public void ShowBuyPanel()
    {
        RerollPanels();

        buyPanel.SetActive(true);
    }

    public void RerollPanels()
    {
        Bonus[] bonuses = Bonus.GetRandomBonuses(buyBonusPanels.Length);
        int i = 0;

        foreach (BuyBonusPanel panel in buyBonusPanels)
        {
            panel.bonus = bonuses[i++];
            panel.gameObject.SetActive(true);
        }
    }
}
