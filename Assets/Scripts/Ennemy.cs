using System;
using System.Collections;
using System.Collections.Generic;
using Spriter2UnityDX;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Ennemy : MonoBehaviour, IPointerClickHandler
{
    public enum EType
    {
        Default,
        Boss,
        NumTypes
    }

    public UnityEvent<Ennemy> OnDeath = new();
    public UnityEvent<Ennemy> OnClicked = new();
    public float Health, MaxHealth;
    public int GoldReward = 1;
    public EType Type = EType.Default;
    public bool Spawned { get => spawned; }

    Player player;
    Animator animator;
    AnimationController animationController;
    UIController ui;
    GameController gameController;
    bool spawned = false;

    public void Start()
    {
        ui = GameObject.Find("UIController").GetComponent<UIController>();
        gameController = GameObject.Find("GameController").GetComponent<GameController>();

        animator = GetComponent<Animator>();
        animationController = animator.GetBehaviour<AnimationController>();
        animationController.OnAnimationEnd.AddListener(OnAnimationEnd);

        GetComponent<EntityRenderer>().SortingOrder = 1;

        if (Type ==  EType.Default)
        {
            MaxHealth = 100f;
        }
        else if (Type == EType.Boss)
        {
            MaxHealth = 300f;
        }

        MaxHealth *= gameController.HealthModifier;

        GoldReward = Mathf.RoundToInt(MaxHealth / 100f);

        if (Type == EType.Boss && GoldReward < 5)
        {
            GoldReward = 5;
        }

        player = GameObject.Find("Player").GetComponent<Player>();
        OnClicked.AddListener(player.OnEnnemyClicked);

        ui.MaxHealth = MaxHealth;
        SetHealth(MaxHealth);

        spawned = true;
    }

    void Update()
    {
        
    }
    
    public void TakeDamage(float amount)
    {
        if (!spawned || amount == 0f || Health == 0f)
        {
            return;
        }

        SetHealth(Math.Max(Health - amount, 0f));

        if (Health > 0f)
        {
            if (!animator)
            {
                animator = GetComponent<Animator>();
            }

            animator.SetTrigger("HurtTrigger");
        }
        else
        {
            Die();
        }
    }

    void Die()
    {
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }

        animator.SetTrigger("DieTrigger");
    }

    void OnAnimationEnd(AnimatorStateInfo state)
    {
        if (state.IsName("Dying"))
        {
            Destroy(gameObject);
            OnDeath.Invoke(this);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClicked.Invoke(this);
    }

    public void SetHealth(float health)
    {
        Health = health;

        if (!ui)
        {
            ui = GameObject.Find("UIController").GetComponent<UIController>();
        }

        ui.Health = Health;
    }
}
