using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BuyPanel : MonoBehaviour
{
    Player player;
    GameController controller;
    Button rerollButton;

    void Start()
    {
        gameObject.SetActive(false);

        controller = GameObject.Find("GameController").GetComponent<GameController>();
        player = GameObject.Find("Player").GetComponent<Player>();

        rerollButton = transform.Find("RerollButton").GetComponent<Button>();
        rerollButton.onClick.AddListener(RerollClicked);

        transform.Find("ConfirmButton").GetComponent<Button>().onClick.AddListener(ConfirmClicked);
        transform.Find("RerollButton/RerollCost").GetComponent<TMP_Text>().text = controller.RerollCost.ToString();
    }

    void Update()
    {
        rerollButton.enabled = player.Gold >= controller.RerollCost;
    }

    void RerollClicked()
    {
        player.Gold -= controller.RerollCost;
        controller.ui.RerollPanels();
    }

    void ConfirmClicked()
    {
        gameObject.SetActive(false);
        controller.NextStage();
    }
}
