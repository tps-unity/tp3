using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageIndicator : MonoBehaviour
{
    public float Duration = 1f;
    public float Speed = 80f;
    public float Damage;
    public Vector2 StartPosition;
    public bool IsCritical = false;

    float elapsedTime;
    TMP_Text text;

    void Start()
    {
        text = GetComponent<TMP_Text>();
        text.text = Mathf.RoundToInt(Damage).ToString();
        text.alpha = 1f;
        text.raycastTarget = false;
        transform.position = StartPosition;

        if (IsCritical)
        {
            text.color = new Color(252f / 255f, 186f / 255f, 3f / 255f);
        }
    }

    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (elapsedTime >= Duration)
        {
            Destroy(gameObject);
        }

        transform.Translate(Vector2.up * Speed * Time.deltaTime);
        text.alpha = 1f - (elapsedTime / Duration);
    }
}
