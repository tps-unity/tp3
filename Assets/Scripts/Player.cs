using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    [SaveField] public float BaseDamage = 10f;
    [SaveField] public float BonusDamage = 0f;
    public float HitCooldown { get => GetHitCooldown(); }
    [SaveField] public float BaseHitCooldown = 0.5f;
    [SaveField] public float HitCooldownReduction = 0f;
    [SaveField] public float Gold = 0f;
    [SaveField] public float PassiveDamage = 0f;
    [SaveField] public float CritChance = 0f;
    [SaveField] public float CritMultiplier = 1.5f;
    [SaveField] public float GoldChance = 0f;
    public float PassiveDamageFreq = 1f;
    public DamageIndicator DamageIndicatorPrefab;

    float timeSinceLastHit = 0f;
    float timeSinceLastPassiveDamage = 0f;
    TMP_Text goldText;
    TMP_Text goldChanceText;
    TMP_Text passiveDamageText;
    TMP_Text critChanceText;
    TMP_Text BonusDamageText;
    Canvas canvas;

    void Start()
    {   
        goldChanceText = GameObject.Find("GoldChance").GetComponent<TMP_Text>();
        goldText = GameObject.Find("GoldAmount").GetComponent<TMP_Text>();
        passiveDamageText = GameObject.Find("PassiveDamage").GetComponent<TMP_Text>();
        critChanceText = GameObject.Find("CritChance").GetComponent<TMP_Text>();
        BonusDamageText = GameObject.Find("BonusDamage").GetComponent<TMP_Text>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    }

    void Update()
    {
        timeSinceLastHit += Time.deltaTime;

        goldText.text = Gold.ToString();
        goldChanceText.text = $"+{Mathf.RoundToInt(GoldChance * 100f).ToString()}%";
        passiveDamageText.text = $"+{PassiveDamage.ToString()}";
        critChanceText.text = $"+{Mathf.RoundToInt(CritChance * 100f).ToString()}%";
        BonusDamageText.text = $"+{BonusDamage.ToString()}";


        /* Passive Damage */

        timeSinceLastPassiveDamage += Time.deltaTime;

        if (PassiveDamage > 0f && timeSinceLastPassiveDamage >= PassiveDamageFreq)
        {
            var ennemy = FindFirstObjectByType(typeof(Ennemy));

            if (ennemy)
            {
                ennemy.GetComponent<Ennemy>().TakeDamage(PassiveDamage);
                timeSinceLastPassiveDamage = 0f;
            }
        }
    }

    public float GetDamage(out bool IsCrit)
    {
        float critMultiplier = UnityEngine.Random.Range(0f, 1f) <= CritChance ? CritMultiplier : 1f;

        IsCrit = critMultiplier > 1f;

        return (BaseDamage + BonusDamage) * critMultiplier;
    }

    public void OnEnnemyClicked(Ennemy ennemy)
    {
        if (timeSinceLastHit < HitCooldown)
        {
            return;
        }

        float damage = GetDamage(out bool isCrit);

        DamageIndicator damageIndicator = Instantiate(DamageIndicatorPrefab, canvas.transform);
        damageIndicator.StartPosition = Input.mousePosition / canvas.scaleFactor;
        damageIndicator.Damage = damage;
        damageIndicator.IsCritical = isCrit;

        ennemy.TakeDamage(damage);
        timeSinceLastHit = 0f;
    }
    
    public void BuyBonus(Bonus bonus)
    {
        Gold -= bonus.Cost;

        switch (bonus.Type)
        {
            case Bonus.EType.Damage:
                BonusDamage += bonus.Value;
                break;

            case Bonus.EType.PassiveDamage:
                PassiveDamage += bonus.Value;
                break;

            case Bonus.EType.BonusGoldChance:
                GoldChance += bonus.Value;
                break;

            case Bonus.EType.CritChance:
                CritChance += bonus.Value;
                break;

            case Bonus.EType.CritDamage:
                CritMultiplier += bonus.Value;
                break;

            case Bonus.EType.HitCooldownReduction:
                HitCooldownReduction += bonus.Value;
                break;
        }
    }

    float GetHitCooldown()
    {
        return BaseHitCooldown * (1f - HitCooldownReduction);
    }
}
